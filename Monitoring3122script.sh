#!/bin/bash
# Script to monitor the following metrics in Glassfish 3.1.2.2 for NGA including:
# 1. web session request statistics
# 2. Request processing metrics
# 3. http thread pool usage


# Set these to appropriate values

servertarget=server
virtualservername=server
#passwordfilelocation="/home/dwinters/password.txt"
GF_HOME=/opt/glassfish3/glassfish/bin


# Script usage parameters

if [ $# -eq 0 ]; then
    echo >&2 "Usage: monitoringstats.sh <count> <delay> <servername> <virtualservername> <passwordfiellocation> <glassfish home location"
    exit 1
fi

# turn on access logging and relevant stats levels

echo "Turning on access logging and monitoring thresholds"  >> metrics.txt

$GF_HOME//asadmin set server.http-service.access-logging-enabled=true

$GF_HOME//asadmin set server.monitoring-service.module-monitoring-levels.http-service=HIGH
$GF_HOME//asadmin set  server.monitoring-service.module-monitoring-levels.web-container=HIGH
$GF_HOME//asadmin set  server.monitoring-service.module-monitoring-levels.thread-pool=HIGH

#echo "Turning off access logging and monitoring thresholds"  >> metrics.txt

#$GF_HOME//asadmin set server.http-service.access-logging-enabled=false

#$GF_HOME//asadmin set server.monitoring-service.module-monitoring-levels.http-service=OFF
#$GF_HOME//asadmin set  server.monitoring-service.module-monitoring-levels.web-container=OFF
#$GF_HOME//asadmin set  server.monitoring-service.module-monitoring-levels.thread-pool=OFF

count=${2:-10}  # defaults to 10 times
delay=${3:-1} # defaults to 600 second




while [ $count -gt 0 ]
do


now="$(date +'%m-%d-%Y-%T')"


# Executing Core asadmin commands



echo Application web session statistics at $now  >> metrics.txt

# The current number of active sessions along with the high and low session values

$GF_HOME/asadmin --user admin  --passwordfile $passwordfilelocation get -m $servertarget.web.session.* >> metrics.txt

echo Application http request statistics at $now >> metrics.txt

# The average and maximum http request processing time

$GF_HOME/asadmin --user admin  --passwordfile $passwordfilelocation get -m $servertarget.http-service.$virtualservername.request.processingtime-description >> metrics.txt
$GF_HOME/asadmin --user admin  --passwordfile $passwordfilelocation get -m $servertarget.http-service.$virtualservername.request.processingtime-lastsampletime >> metrics.txt
$GF_HOME/asadmin --user admin  --passwordfile $passwordfilelocation get -m $servertarget.http-service.$virtualservername.request.maxtime-description >> metrics.txt
$GF_HOME/asadmin --user admin  --passwordfile $passwordfilelocation get -m $servertarget.http-service.$virtualservername.request.maxtime-lastsampletime >> metrics.txt

# Capture thread pool stats

echo Http listener 1 thread pool statistics at $now >> metrics.txt

$GF_HOME/asadmin --user admin  --passwordfile $passwordfilelocation get -m $servertarget.network.http-listener-1.thread-pool.* >> metrics.txt

echo Http listener 2 thread pool statistics at $now >> metrics.txt

$GF_HOME/asadmin --user admin  --passwordfile $passwordfilelocation get -m $servertarget.network.http-listener-2.thread-pool.* >> metrics.txt


sleep $delay
let count--
echo -n "."
done


echo Exiting after completing monitoring tasks >> metrics.txt


# Exiting Script



